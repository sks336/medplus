package com.proj.medplus.server.common.service;

import com.proj.medplus.vo.common.DummyVO;

/**
 * Created by Shukla, Sachin. on 3/8/15.
 */
public interface IPersonService {

    public DummyVO getDummyObj();
}
