package com.proj.medplus.server.common.service;

import com.proj.medplus.vo.common.DummyVO;
import org.springframework.stereotype.Component;

/**
 * Created by Shukla, Sachin. on 3/8/15.
 */

@Component
public class PersonService implements IPersonService{


    @Override
    public DummyVO getDummyObj() {
        return DummyVO.getObj();
    }
}
